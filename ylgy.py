import sys
import json
import os
from pathlib import Path, PureWindowsPath
import logging

logging.basicConfig(level = logging.INFO,format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

def read_json(db):
    try:
        with open(db) as f:
            data = json.load(f)
    except FileNotFoundError:
        return {}
        
    return data

def save_json(db, data):
    if data:
        with open(db, 'w+', encoding='utf-8') as f:
            json.dump(data, f, indent=4, separators=(',', ':'),ensure_ascii=False)

def read_replaced_data(j = "replaced_data.json"): 
    # 读取需要替换的文件数据

    # replaced_data.json 里面的数据是啥，我也不清楚
    replaced_data = """
    [1,0,0,[["cc.JsonAsset",["_name","json"],1]],[[0,0,1,3]],[[0,"levelConfigData",{"dailyLevel":[[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001],[80001,80001]],"topicLevel":[[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017],[10017,10017]]}]],0,0,[],[],[]]
    """
    return json.loads(replaced_data)

def check_json(j): 
    # 匹配方法
    # print("check_json",j)

    size = os.path.getsize(j)

    # print("check_json", j,size)

    if size > 1024 * 1 and size <=  1024 * 2: # 通过文件大小判断
        return True

    return False

# 这个路径复制过来的，我也不知道对不对
def main(path = PureWindowsPath("\\Applet\\wx141bfb9b73c970a9\\usr\\gamecaches\\resources")):
    path = Path(path)

    jsons = os.listdir(path)

    j_ok = []
    for j in jsons:
        if '.json' in j:
            # print("main", j)
            if check_json(path / j):
                j_ok.append(j)

    # print("main: len j_ok", len(j_ok))
    if len(j_ok) > 1:
        logger.error("""
        匹配到的文件过多,请检查匹配方法
        """)
        exit(-1)
    elif len(j_ok) == 0:
        logger.error("""
        未匹配到对应的文件,请检查匹配方法
        """)
        exit(-2)

    j_ok = path / j_ok[0]
    save_json(j_ok, read_replaced_data())
    logger.info(f"""
    成功匹配到文件{j_ok},修改成功
    """)
    
if __name__ == '__main__':
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        main()